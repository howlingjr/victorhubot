//
//  ShifumiViewController.swift
//  VictorHubot
//
//  Created by Ludovic Grimbert on 23/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import UIKit

enum Position: Int {
    case rock = 1
    case paper = 2
    case scissors = 3
    case initial = 0
    case initialFromRock = 4
    case initialFromPaper = 5
    case initialFromScissors = 6
}


class ShifumiViewController: ViewController {
    
    @IBOutlet weak var startButton: CircleButton!
    
    var brick : Ev3Brick?
    var timer : Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.moveFingers(to: .initial)
    }
    
    func turnMotor(ports: OutputPort, speed: Int16, milliseconds: UInt32, brake: Bool) {
        guard let brick = self.brick else {return}
        let c = Ev3Command(commandType: .directNoReply)
        c.turnMotorAtSpeedForTime(ports: ports, speed: speed, milliseconds: milliseconds, brake: brake)
        brick.sendCommand(c)
    }
    
    @IBAction func startGame(_ sender: Any) {
        
        self.start(intervalResetting: 3)
    }
    
    func start(intervalResetting: Double) {
        
        /// Choice random integer
        let randomInt = Int.random(in: 1...3)
        
        /// Display position with random integer
        self.pickPosition(randomInt: randomInt, isResetting: false)
        
        /// Display default position after interval
        self.timer = Timer.scheduledTimer(withTimeInterval: intervalResetting, repeats: false, block: { _ in
            self.pickPosition(randomInt: randomInt , isResetting: true)
        })
    }
    
    func pickPosition(randomInt: Int, isResetting: Bool) {
        
        switch (randomInt,isResetting){
        case (1, false) : self.moveFingers(to: .rock)
        case (2, false): self.moveFingers(to: .paper)
        case (3, false): self.moveFingers(to: .scissors)
        case (1, true): self.moveFingers(to: .initialFromRock)
        case (2, true): self.moveFingers(to: .initialFromPaper)
        case (3, true): self.moveFingers(to: .initialFromScissors)
        default:
            break
        }
    }
    
    func moveFingers(to position: Position) {
        
        print("")
        
        switch position {
        case .initial:
            self.turnMotor(ports: [.B,.C], speed: 10, milliseconds: 400, brake: false)
            print("Initial 🤖")
        case .paper:
            self.turnMotor(ports: [.B,.C], speed: -10, milliseconds: 400, brake: false)
            print("Paper 🤖")
        case .rock:
            self.turnMotor(ports: [.B,.C], speed: 10, milliseconds: 400, brake: false)
            print("Rock 🤖")
        case .scissors:
            self.turnMotor(ports: [.C], speed: -10, milliseconds: 400, brake: false)
            print("Scissors 🤖")
        case .initialFromRock:
            self.turnMotor(ports: [.B,.C], speed: -10, milliseconds: 400, brake: false)
            print("Initial From Rock 🤖")
        case .initialFromPaper:
            self.turnMotor(ports: [.B,.C], speed: 10, milliseconds: 400, brake: false)
            print("Initial From Paper 🤖")
        case .initialFromScissors:
            self.turnMotor(ports: [.C], speed: 10, milliseconds: 400, brake: false)
            print("Initial From Scissors 🤖")
        }
        print("")
    }
    
    func stopGame(){
        
    }
    
    
}
