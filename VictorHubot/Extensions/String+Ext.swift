//
//  String+Ext.swift
//  VictorHubot
//
//  Created by Ludovic Grimbert on 19/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import Foundation

extension String {
    func convertToDistanceRange() -> String {
        switch self {
        case "0": return "Entre 0 et 10 cm"
        case "1": return "Entre 10 et 20 cm"
        case "2": return "Entre 20 et 30 cm"
        case "3": return "Entre 30 et 40 cm"
        case "4": return "Entre 40 et 50 cm"
        case "5": return "Entre 50 et 60 cm"
        default: return "Plus de 60 cm"
        }
    }
}
