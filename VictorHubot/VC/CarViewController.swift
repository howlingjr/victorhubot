//
//  ViewController.swift
//  VictorHubot
//
//  Created by Ludovic Grimbert on 12/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import UIKit
import ExternalAccessory

class CarViewController: UIViewController {
    
    @IBOutlet weak var proximityLabel: UILabel!
    
    var brick : Ev3Brick?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    
    @IBAction func displayProximity(_ sender: Any) {
        guard let brick = self.brick else{
            print("NO Brick")
            return
        }
        
        brick.directCommand.readyRaw(port: .one, mode: 0, receivedRaw: { data in
            guard let description = data?.debugDescription else {return}
            let distanceRaw = String(description.replacingOccurrences(of: "<", with: "").prefix(1))
            self.proximityLabel.text = distanceRaw.convertToDistanceRange()
        })
    }
    
    
    @IBAction func go(_ sender: Any) {
        guard let brick = self.brick else {return}
        brick.directCommand.turnMotorAtSpeed(onPorts: [.B,.C], withSpeed: 100)
    }
    
    @IBAction func stop(_ sender: Any) {
        guard let brick = self.brick else {return}
        brick.directCommand.stopMotor(onPorts: [.B,.C], withBrake: true)
    }
    
    @IBAction func goBack(_ sender: Any) {
        guard let brick = self.brick else {return}
        brick.directCommand.turnMotorAtSpeed(onPorts: [.B,.C], withSpeed: -100)
    }
    
    @IBAction func goRight(_ sender: Any) {
        guard let brick = self.brick else {return}
        let c = Ev3Command(commandType: .directNoReply)
        c.turnMotorAtSpeedForTime(ports: .B, speed: -100, milliseconds: 2000, brake: false)
        c.turnMotorAtSpeedForTime(ports: .C, speed: 100, milliseconds: 2000, brake: false)
        brick.sendCommand(c)
    }
    
    @IBAction func goLeft(_ sender: Any) {
        guard let brick = self.brick else {return}
        let c = Ev3Command(commandType: .directNoReply)
        c.turnMotorAtSpeedForTime(ports: .B, speed: 100, milliseconds: 2000, brake: false)
        c.turnMotorAtSpeedForTime(ports: .C, speed: -100, milliseconds: 2000, brake: false)
        brick.sendCommand(c)
    }
    
}

