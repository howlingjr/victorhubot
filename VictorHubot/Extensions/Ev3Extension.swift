//
//  EV3.swift
//  VictorHubot
//
//  Created by Ludovic Grimbert on 12/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import Foundation
import ExternalAccessory
import UIKit

class Ev3Extension {
    
    var brick : Ev3Brick?
    var connection : Ev3Connection?
    let man = EAAccessoryManager.shared()
    
    func checkEv3Accessory(withSetting isOpen: Bool) -> Ev3Brick? {
        guard let eAAccessory = getEv3Accessory() else {
            print("Not connected 🔌")
            if isOpen { self.launchSettings() }
            return nil
        }
        
        return self.connect(accessory: eAAccessory)
    }
    
    /// Open Bluetooth and connect EV3 -> OUI
    func launchSettings() {
        print("Launch Settings")
        guard let url = URL(string: "App-Prefs:root=General") else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func getEv3Accessory() -> EAAccessory? {
        //TODO
       // let man = EAAccessoryManager.shared()

        print("Test Ev3 Accessory")
        let connected = man.connectedAccessories
        
        for tmpAccessory in connected{
            if Ev3Connection.supportsEv3Protocol(accessory: tmpAccessory){
                print("Get Ev3 Accessory")
                return tmpAccessory
            }
        }
        return nil
    }
    
    func connect(accessory: EAAccessory) -> Ev3Brick? {
        self.connection = Ev3Connection(accessory: accessory)
        guard let connection = self.connection else {
            print("NO Connection")
            return nil
        }
        self.brick = Ev3Brick(connection: connection)
        self.connection?.open()
        return self.brick
    }
}

