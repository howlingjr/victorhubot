//
//  HomeViewController.swift
//  VictorHubot
//
//  Created by ludovic grimbert on 26/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import UIKit

protocol HomeViewControllerDelegate: class {
    func displayProject(brick: Ev3Brick?, index: Int)
}

class HomeViewController: ViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    
    weak var delegate: HomeViewControllerDelegate?
    
    var brick : Ev3Brick?
    var ev3Extension : Ev3Extension? = Ev3Extension()
    
    var projects = [Project]() {
        didSet {
            DispatchQueue.main.async {
                self.homeTableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.connectBrick()
     //   self.readInfoFromBrick()
        
        homeTableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        homeTableView.tableFooterView = UIView(frame: .zero)
        
        projects = [Project(title: "Shi Fu Mi"),
                    Project(title: "Car")]
    }
    
    func connectBrick() {
        self.brick = self.ev3Extension?.checkEv3Accessory(withSetting: true)
    }
    
    func readInfoFromBrick() {
        
        let c = Ev3Command(commandType: .directNoReply)
        
        c.getBatteryLevel(index: 0)
        c.getFirwmareVersion(maxLength: 0, index: 0)
    }
    
}


extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        let item = self.projects[indexPath.row]
        cell.project = item
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.displayProject(brick: self.brick, index: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

