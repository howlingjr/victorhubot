//
//  ViewController.swift
//  Test1AdScientiam
//
//  Created by ludovic grimbert on 15/03/2019.
//  Copyright © 2019 ludovic grimbert. All rights reserved.
//

import Foundation
import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    self.view.backgroundColor = #colorLiteral(red: 0, green: 0.3625269532, blue: 0.5761555433, alpha: 1)
  }

}
