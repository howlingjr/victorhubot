//
//  MainCoordinator.swift
//  VictorHubot
//
//  Created by ludovic grimbert on 26/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import Foundation
import UIKit

public protocol Coordinator : class {
    var childCoordinators: [Coordinator] { get set }
    init(navigationController:UINavigationController)
    func start()
}

class MainCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    
    unowned let navigationController: UINavigationController
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        
        let homeViewController = HomeViewController.fromStoryboard(name: "Main")
        homeViewController.title = "LEGO EV3"
        homeViewController.delegate = self
        self.navigationController.viewControllers = [homeViewController]
        
    }
}

extension MainCoordinator: HomeViewControllerDelegate {
    func displayProject(brick: Ev3Brick?, index: Int) {
        
        switch index {
        case 0:
            self.displayShifumi(brick: brick)
        case 1:
            self.displayCar(brick: brick)
        default:
            break
        }
    }
    
    
    func displayShifumi(brick: Ev3Brick?) {
        let shifumiViewController = ShifumiViewController.fromStoryboard(name: "Main")
        shifumiViewController.title = "Shi Fu Mi"
        shifumiViewController.brick = brick
        navigationController.pushViewController(shifumiViewController, animated: true)
    }
    
    func displayCar(brick: Ev3Brick?) {
        let carViewController = CarViewController.fromStoryboard(name: "Main")
        carViewController.title = "Car"
        carViewController.brick = brick
        navigationController.pushViewController(carViewController, animated: true)
    }
}

