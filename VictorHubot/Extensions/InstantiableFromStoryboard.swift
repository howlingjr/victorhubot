//
//  InstantiableFromStoryboard.swift
//  VictorHubot
//
//  Created by ludovic grimbert on 26/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import Foundation
import UIKit

protocol InstantiableFromStoryboard {}

extension InstantiableFromStoryboard {
  static func fromStoryboard(name: String, bundle: Bundle? = nil) -> Self {
    let identifier = String(describing: self)
    guard let viewController = UIStoryboard(name: name, bundle: bundle).instantiateViewController(withIdentifier: identifier) as? Self else {
      fatalError("Cannot instantiate view controller of type " + identifier)
    }
    return viewController
  }
}

extension UIViewController: InstantiableFromStoryboard {}
