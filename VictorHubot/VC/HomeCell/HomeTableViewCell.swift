//
//  HomeTableViewCell.swift
//  VictorHubot
//
//  Created by ludovic grimbert on 26/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: Label!
    
    
    var project: Project? {
        didSet {
            if let title = project?.title {
                titleLabel.text = "\(title)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = #colorLiteral(red: 0.2325934172, green: 0.5303766131, blue: 0.7023907304, alpha: 1)
        self.selectionStyle = .none
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
