//
//  UIButton.swift
//  VictorHubot
//
//  Created by ludovic grimbert on 26/04/2020.
//  Copyright © 2020 Ludovic Grimbert. All rights reserved.
//

import Foundation
import UIKit


extension UINavigationBar {
    
    public class func configureAppearance() {
        
        
        let appearance = self.appearance()
        appearance.isTranslucent = false
        appearance.barTintColor =  #colorLiteral(red: 0, green: 0.2903347313, blue: 0.4638395309, alpha: 1)
        appearance.tintColor =  #colorLiteral(red: 0.8869613409, green: 0.9319426417, blue: 0.9557299018, alpha: 1)
        
        appearance.setBackgroundImage(UIImage(), for: .default)
        appearance.shadowImage = UIImage()
        
        guard let image =  UIImage(named : "nav_back") else {return}
        appearance.backIndicatorImage = image.imageFlippedForRightToLeftLayoutDirection()
        appearance.backIndicatorTransitionMaskImage = image.imageFlippedForRightToLeftLayoutDirection()
        
        
        guard let font: UIFont = UIFont(name: "Avenir-book", size: 20) else {return}
        let titleTextAttributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: UIColor.white]
        appearance.titleTextAttributes = titleTextAttributes
        
        
    }
}

class CircleButton : UIButton {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.titleLabel?.font = UIFont(name: "Avenir-book", size: 16)!
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
        self.backgroundColor = #colorLiteral(red: 0.2325934172, green: 0.5303766131, blue: 0.7023907304, alpha: 1)
        self.setTitleColor( #colorLiteral(red: 0.8869613409, green: 0.9319426417, blue: 0.9557299018, alpha: 1), for: .normal)
        self.layer.borderWidth = 20
        self.layer.borderColor =  #colorLiteral(red: 0, green: 0.2903347313, blue: 0.4638395309, alpha: 1)
    }
    
}

class TableView : UITableView {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.backgroundColor =  #colorLiteral(red: 0.5186650157, green: 0.6251547933, blue: 0.7313920259, alpha: 1)
        self.tintColor = UIColor.clear
        self.isScrollEnabled = false
    }
    
}

class Label : UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.font = UIFont(name: "Avenir-book", size: 16)!
        self.textColor =  #colorLiteral(red: 0.8869613409, green: 0.9319426417, blue: 0.9557299018, alpha: 1)
    }
    
}
